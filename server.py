#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import socketserver
import sys
import json
from time import time, strftime, gmtime

class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}

    def register2json(self):
            fichero = 'registered.json'
            with open(fichero,'w') as fichero_json:
                json.dump(self.dicc, fichero_json, indent=1)

    def handle(self):
        """

        handle method of the server class
        (all requests will be handled by this method)
        """
        self.wfile.write(b"Hemos recibido tu peticion")
        dir_ip= self.client_address[0]
        puerto= self.client_address[1]
        line = self.rfile.read()
        REGISTER= line.decode('utf-8').split(' ')[0]
        USUARIO= line.decode('utf-8').split(' ')[2]
        CADUCIDAD= line.decode('utf-8').split(' ')[3].split('\r\n')[1].split(':')[1]
        tiempo = ' Expires: ' + CADUCIDAD
        usr_dicc= ('Direccion ' + str(dir_ip) + ' Puerto ' + str(puerto) + tiempo)
        crono = int(CADUCIDAD) + int(time())
        tiempo = strftime('%Y-%m-%d %H:%M:%S',gmtime(crono))

        for line in self.rfile:
            print("El cliente nos manda ", line.decode('utf-8'))

        if REGISTER == 'register':
            self.dicc[USUARIO] = usr_dicc, tiempo

        if int(CADUCIDAD) == 0:
            print('Deleteing ' + USUARIO)
            del self.dicc[USUARIO]
        print(usr_dicc)
        self.register2json()
        print(self.dicc)



if __name__ == "__main__":
    puerto_servidor = int(sys.argv[1])
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('',puerto_servidor), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")

#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = sys.argv[1]
PORT = int(sys.argv[2])
# LINE = str(sys.argv[5:])
USUARIO = str(sys.argv[4])
REGISTER = str(sys.argv[3])
CADUCIDAD = int(sys.argv[5])
usr_dicc = REGISTER + ' sip: ' + USUARIO + ' SIP/2.0' '\r\n' + \
           'Expires:' + str(CADUCIDAD) + '\r\n'
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", usr_dicc)
    my_socket.send(bytes(usr_dicc, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
